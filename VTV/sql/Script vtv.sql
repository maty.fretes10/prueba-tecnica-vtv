DROP DATABASE IF EXISTS  vtv;
CREATE database vtv;

CREATE TABLE `vtv`.`inspector` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
  
CREATE TABLE `vtv`.`vehiculo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `dominio` VARCHAR(45) NOT NULL,
  `propietario` VARCHAR(45) NOT NULL,
  `estado` VARCHAR(45) NOT NULL,
  `marca` VARCHAR(45) NOT NULL,
  `modelo` VARCHAR(45) NOT NULL,
  `vto` VARCHAR(45),
  PRIMARY KEY (`id`));
  
CREATE TABLE `vtv`.`inspeccion`(
`id` INT NOT NULL AUTO_INCREMENT,
`fecha` VARCHAR(45) NOT NULL,
`estado` VARCHAR(45) NOT NULL,
`exento` BOOLEAN NOT NULL,
`id_vehiculo` INT NOT NULL,
`id_inspector` INT NOT NULL,
PRIMARY KEY (`id`),

constraint fk_vehiculo_inspeccion foreign key (id_vehiculo) references vehiculo(id),
constraint fk_inspector_inspeccion foreign key (id_inspector) references inspector(id)
);

INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(1, "Matias Fretes");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(2, "Juan Gonzalez");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(3, "Luis Fernandez");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(4, "Jose Diaz");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(5, "Esteban Perez");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(6, "Pablo Gomez");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(7, "Nicolas Lucero");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(8, "Martin Sosa");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(9, "Luciano Fort");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(10, "Joaquin Acosta");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(11, "Kevin Dominguez");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(12, "Jonas Acosta");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(13, "Julio Diaz");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(14, "Milton Lopez");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(15, "Mateo Sosa");
INSERT INTO `vtv`.`inspector` (id, nombre) VALUES(16, "Marcos Beltran");

INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(1,"LQA887","Matias Fretes","APTO","TOYOTA","ETIOS","9/6/2023");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(2,"AFS412","Jose Suarez","RECHAZADO","HONDA","CIVIC","6/12/2021");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(3,"HGF563","Hernan Barco","CONDICIONAL","FORD","FIESTA","15/9/2022");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(4,"AC025CD","Lionel Castro","APTO","CHEVROLET","AVEO","8/7/2021");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(5,"HGD548","Lucas Perez","APTO","TOYOTA","HILUX","20/10/2022");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(6,"POI123","Carlos Pereyra","APTO","TOYOTA","COROLLA","5/2/2023");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(7,"LPY345","Matias Fretes","APTO","CHEVROLET","ASTRA","6/6/2022");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(8,"OOQ542","Matias Fretes","CONDICIONAL","RENAULT","SANDERO","4/3/2022");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(9,"LLN785","Enzo Pintos","APTO","RENAULT","LOGAN","5/5/2022");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(10,"REX549","Jose Suarez","RECHAZADO","CHEVROLET","CORSA","12/5/2021");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(11,"ABC287","Jose Lopez","RECHAZADO","CHEVROLET","AVEO","28/5/2022");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(12,"HRE754","Milton Bueno","APTO","FORD","KA","12/6/2023");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(13,"KKY342","Pedro Martinez","APTO","PEUGEOT","308","15/2/2023");
INSERT INTO `vtv`.`vehiculo` (id, dominio, propietario, estado, marca, modelo, vto) VALUES(14,"FOI45","Octavio Flores","CONDICIONAL","CHEVROLET","CORSA","10/6/2022");

INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(1, "5/2/2022", "APTO", false, 6, 2);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(2, "12/5/2021", "RECHAZADO", true, 10, 2);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(3, "3/3/2022", "CONDICIONAL", false, 8, 3);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(4, "6/6/2021", "APTO", false, 7, 2);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(5, "9/6/2022", "APTO", false, 1, 8);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(6, "6/12/2021", "RECHAZADO", false, 2, 2);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(7, "8/7/2022", "APTO", true, 4, 8);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(8, "16/9/2022", "CONDICIONAL", false, 3, 1);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(9, "5/5/2021", "APTO", false, 9, 1);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(10, "20/10/2021", "APTO", true, 5, 4);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(11, "9/6/2022", "CONDICIONAL", true, 14, 4);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(12, "12/6/2022", "APTO", false, 12, 5);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(13, "15/2/2022", "APTO", true, 13, 3);
INSERT INTO `vtv`.`inspeccion` (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(14, "28/5/2022", "RECHAZADO", false, 11, 2);






