package main;

import java.awt.EventQueue;
import interfaz.Vtv;
import persistencia.Conexion;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					@SuppressWarnings("unused")
					Conexion conexion = new Conexion();
					Vtv window = new Vtv();
					window.show();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
