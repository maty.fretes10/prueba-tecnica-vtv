package dto;

public class VehiculoDto {

	private int idVehiculo;
	private String dominio;
	private String propietario;
	private Estado estado;
	private String marca;
	private String modelo;
	private String vto;

	public VehiculoDto(int id, String dom, String prop, Estado est, String marca, String modelo, String vto) {
		this.idVehiculo = id;
		this.dominio = dom;
		this.propietario = prop;
		this.estado = est;
		this.marca = marca;
		this.modelo = modelo;
		this.vto = vto;
	}

	public int getIdVehiculo() {
		return idVehiculo;
	}

	public void setIdVehiculo(int idVehiculo) {
		this.idVehiculo = idVehiculo;
	}

	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getVto() {
		return vto;
	}

	public void setVto(String vto) {
		this.vto = vto;
	}

}
