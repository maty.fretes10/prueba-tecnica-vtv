package dto;

public class InspectorDto {
	
	private int idInspector;
	private String nombre;
	
	public InspectorDto(int id, String nombre) {
		this.idInspector =id;
		this.nombre = nombre;
	}

	public int getIdInspector() {
		return idInspector;
	}

	public void setIdInspector(int idInspector) {
		this.idInspector = idInspector;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return idInspector + " - " + nombre;
	}

	

	
}
