package dto;

public class InspeccionDto {

	private int idInspeccion;
	private String fecha;
	private Estado estado;
	private boolean exento;
	private int idVehiculo;
	private int idInspector;

	public InspeccionDto(int id, String fecha, Estado estado, boolean exc, int idVehiculo, int idInspector) {
		this.idInspeccion = id;
		this.fecha = fecha;
		this.estado = estado;
		this.exento = exc;
		this.idVehiculo = idVehiculo;
		this.idInspector = idInspector;
	}

	public int getIdInspeccion() {
		return idInspeccion;
	}

	public void setIdInspeccion(int idInspeccion) {
		this.idInspeccion = idInspeccion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public boolean isExento() {
		return exento;
	}

	public void setExento(boolean exento) {
		this.exento = exento;
	}

	public int getIdVehiculo() {
		return idVehiculo;
	}

	public void setIdVehiculo(int idVehiculo) {
		this.idVehiculo = idVehiculo;
	}

	public int getIdInspector() {
		return idInspector;
	}

	public void setIdInspector(int idInspector) {
		this.idInspector = idInspector;
	}

}
