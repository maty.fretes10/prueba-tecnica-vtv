package persistencia.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.Conexion;
import dto.Estado;
import dto.InspeccionDto;

public class InspeccionDAOSQL {
	private static final String insert = "INSERT INTO inspeccion (id, fecha, estado, exento, id_vehiculo, id_inspector) VALUES(?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE inspeccion SET fecha = ?, estado = ?, exento = ?, id_vehiculo = ?, id_inspector = ? WHERE id = ?";
	private static final String delete = "DELETE FROM inspeccion WHERE id = ?";
	private static final String readall = "SELECT * FROM inspeccion";
	private static final Conexion conexion = Conexion.getConexion();
	private static final String conseguirID = "SELECT * FROM inspeccion WHERE id = ?";

	public boolean insert(InspeccionDto inspeccion) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, inspeccion.getIdInspeccion());
			statement.setString(2, inspeccion.getFecha());
			statement.setString(3, inspeccion.getEstado().toString());
			statement.setBoolean(4, inspeccion.isExento());
			statement.setInt(5, inspeccion.getIdVehiculo());
			statement.setInt(6, inspeccion.getIdInspector());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return isInsertExitoso;
	}

	public boolean update(InspeccionDto inspeccion) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(update);
			statement.setInt(6, inspeccion.getIdInspeccion());
			statement.setString(1, inspeccion.getFecha());
			statement.setString(2, inspeccion.getEstado().toString());
			statement.setBoolean(3, inspeccion.isExento());
			statement.setInt(4, inspeccion.getIdVehiculo());
			statement.setInt(5, inspeccion.getIdInspector());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isInsertExitoso;
	}

	public boolean delete(int id) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, id);
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isdeleteExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	public List<InspeccionDto> readAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<InspeccionDto> inspecciones = new ArrayList<InspeccionDto>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				inspecciones.add(getInspeccionDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return inspecciones;
	}

	private InspeccionDto getInspeccionDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id");
		String fecha = resultSet.getString("fecha");
		String estado = resultSet.getString("estado");
		boolean exento = resultSet.getBoolean("exento");
		int idVehiculo = resultSet.getInt("id_vehiculo");
		int idInspector = resultSet.getInt("id_inspector");

		return new InspeccionDto(id, fecha, Estado.valueOf(estado), exento, idVehiculo, idInspector);
	}

	public InspeccionDto getById(int id) {

		PreparedStatement statement;
		ResultSet resultSet;
		InspeccionDto inspecciondto = null;
		try {
			statement = conexion.getSQLConexion().prepareStatement(conseguirID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				inspecciondto = new InspeccionDto(resultSet.getInt("id"), resultSet.getString("fecha"),
						Estado.valueOf(resultSet.getString("estado")), resultSet.getBoolean("exento"),
						resultSet.getInt("id_vehiculo"), resultSet.getInt("id_inspector"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return inspecciondto;
	}
}
