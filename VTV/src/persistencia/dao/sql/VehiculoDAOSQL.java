package persistencia.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import persistencia.Conexion;
import dto.Estado;
import dto.VehiculoDto;

public class VehiculoDAOSQL {
	private static final String insert = "INSERT INTO vehiculo (id, dominio, propietario, estado, marca, modelo, vto) VALUES(?, ?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE vehiculo SET dominio = ?, propietario = ?, estado = ?, marca = ?, modelo = ?, vto = ? WHERE id = ?";
	private static final String delete = "DELETE FROM vehiculo WHERE id = ?";
	private static final String readall = "SELECT * FROM vehiculo";
	private static final Conexion conexion = Conexion.getConexion();
	private static final String conseguirID = "SELECT * FROM vehiculo WHERE id = ?";

	public int insert(VehiculoDto vehiculo) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		int idGenerado = 0;
		try {
			statement = conexion.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, vehiculo.getIdVehiculo());
			statement.setString(2, vehiculo.getDominio());
			statement.setString(3, vehiculo.getPropietario());
			statement.setString(4, vehiculo.getEstado().toString());
			statement.setString(5, vehiculo.getMarca());
			statement.setString(6, vehiculo.getModelo());
			statement.setString(7, vehiculo.getVto());
			

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				
				ResultSet generatedKeys = statement.getGeneratedKeys();
				if (generatedKeys.next()) {
					idGenerado = generatedKeys.getInt(1); 
				}
			}
			

			
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}


		
		return idGenerado;
	}

	public boolean update(VehiculoDto vehiculo) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(update);
			statement.setInt(7, vehiculo.getIdVehiculo());
			statement.setString(1, vehiculo.getDominio());
			statement.setString(2, vehiculo.getPropietario());
			statement.setString(3, vehiculo.getEstado().toString());
			statement.setString(4, vehiculo.getMarca());
			statement.setString(5, vehiculo.getModelo());
			statement.setString(6, vehiculo.getVto());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isInsertExitoso;
	}

	public boolean delete(int id) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, id);
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isdeleteExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	public List<VehiculoDto> readAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<VehiculoDto> vehiculos = new ArrayList<VehiculoDto>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				vehiculos.add(getVehiculoDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return vehiculos;
	}

	private VehiculoDto getVehiculoDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id");
		String dominio = resultSet.getString("dominio");
		String propietario = resultSet.getString("propietario");
		String estado = resultSet.getString("estado");
		String marca = resultSet.getString("marca");
		String modelo = resultSet.getString("modelo");
		String vto = resultSet.getString("vto");

		return new VehiculoDto(id, dominio, propietario, Estado.valueOf(estado), marca, modelo, vto);
	}

	public VehiculoDto getById(int id) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		VehiculoDto _vehiculo = null;
		try {
			statement = conexion.getSQLConexion().prepareStatement(conseguirID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				_vehiculo = new VehiculoDto(resultSet.getInt("id"), resultSet.getString("dominio"),
						resultSet.getString("propietario"), Estado.valueOf(resultSet.getString("estado")),
						resultSet.getString("marca"), resultSet.getString("modelo"), resultSet.getString("vto"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return _vehiculo;
	}
}
