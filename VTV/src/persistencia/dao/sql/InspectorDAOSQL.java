package persistencia.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.Conexion;
import dto.InspectorDto;

public class InspectorDAOSQL 
{
	private static final String insert = "INSERT INTO inspector (id, nombre) VALUES(?, ?)";
	private static final String update = "UPDATE inspector SET nombre = ? WHERE id = ?";
	private static final String delete = "DELETE FROM inspector WHERE id = ?";
	private static final String readall = "SELECT * FROM inspector";	
	private static final Conexion conexion = Conexion.getConexion();
	private static final String conseguirID = "SELECT * FROM inspector WHERE id = ?";
	
	public boolean insert(InspectorDto inspector)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, inspector.getIdInspector());
			statement.setString(2, inspector.getNombre());
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean update(InspectorDto inspectores) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(update);
			statement.setInt(2, inspectores.getIdInspector());
			statement.setString(1, inspectores.getNombre());
			
     		if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isInsertExitoso;
	}
	
	public boolean delete(int id)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, id);
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<InspectorDto> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<InspectorDto> inspectores = new ArrayList<InspectorDto>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				inspectores.add(getInspectorDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return inspectores;
	}
	
	private InspectorDto getInspectorDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("Nombre");

		return new InspectorDto(id, nombre);
	}
	
	public InspectorDto getById(int id) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		InspectorDto inspector = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(conseguirID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				inspector = new InspectorDto(resultSet.getInt("id"),
										 resultSet.getString("Nombre"));										 
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return inspector;
	}
}
