package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import dto.InspectorDto;
import interfaz.ventanas.VentanaAuxInspector;

import service.InspectorService;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class Inspector implements ActionListener {
	JFrame frmInspector;
	private JTable tablaInspector;
	private JButton btnAgregar, btnGuardar, btnEditar, btnBorrar;
	private static DefaultTableModel modelInspector;
	private static List<InspectorDto> inspectorEnTabla;
	private static String[] nombreColumnas = { "Inspectores" };
	private VentanaAuxInspector ventanaAuxInspectores;
	static InspectorService inspectorService;
	public static JLabel lblMensaje;

	public Inspector() {
		super();
		initialize();
		this.getBtnAgregar().addActionListener(this);
		this.getBtnBorrar().addActionListener(this);
		this.getBtnEditar().addActionListener(this);
		refrescarTabla();
	}

	private void initialize() {
		frmInspector = new JFrame();
		frmInspector.setResizable(false);
		frmInspector.setBounds(100, 100, 700, 460);
		frmInspector.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 128, 128));
		panel.setBounds(0, 0, 694, 431);
		frmInspector.getContentPane().add(panel);
		panel.setLayout(null);

		JScrollPane spPropietarios = new JScrollPane();
		spPropietarios.setBounds(24, 33, 432, 259);
		spPropietarios.setBackground(new Color(224, 255, 255));
		panel.add(spPropietarios);

		modelInspector = new DefaultTableModel(null, nombreColumnas);
		tablaInspector = new JTable(modelInspector);
		tablaInspector.setRowMargin(0);
		tablaInspector.setForeground(new Color(105, 105, 105));
		tablaInspector.setFont(new Font("Arial", Font.BOLD, 14));
		tablaInspector.setBackground(new Color(255, 255, 255));
		tablaInspector.setGridColor(new Color(105, 105, 105));
		tablaInspector.getColumnModel().getColumn(0).setPreferredWidth(130);
		tablaInspector.getColumnModel().getColumn(0).setResizable(false);

		spPropietarios.setViewportView(tablaInspector);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setForeground(new Color(105, 105, 105));
		btnAgregar.setFont(new Font("Arial", Font.BOLD, 15));
		btnAgregar.setBackground(new Color(224, 255, 255));
		btnAgregar.setBounds(474, 33, 178, 67);
		btnAgregar.setToolTipText("Agregar");
		panel.add(btnAgregar);

		btnEditar = new JButton("Editar");
		btnEditar.setForeground(new Color(105, 105, 105));
		btnEditar.setFont(new Font("Arial", Font.BOLD, 15));
		btnEditar.setBackground(new Color(224, 255, 255));
		btnEditar.setBounds(474, 127, 178, 67);
		btnEditar.setToolTipText("Editar");
		panel.add(btnEditar);

		btnBorrar = new JButton("Eliminar");
		btnBorrar.setForeground(new Color(105, 105, 105));
		btnBorrar.setFont(new Font("Arial", Font.BOLD, 15));
		btnBorrar.setBackground(new Color(224, 255, 255));
		btnBorrar.setBounds(474, 225, 178, 67);
		btnBorrar.setToolTipText("Eliminar");
		panel.add(btnBorrar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setForeground(new Color(105, 105, 105));
		btnGuardar.setFont(new Font("Arial", Font.BOLD, 15));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Vtv.ActualizarInspectores();
				frmInspector.setVisible(false);
			}
		});
		btnGuardar.setToolTipText("Guardar");
		btnGuardar.setBackground(new Color(224, 255, 255));
		btnGuardar.setBounds(134, 334, 235, 67);
		panel.add(btnGuardar);
		
		lblMensaje = new JLabel("");
		lblMensaje.setBackground(new Color(255, 255, 255));
		lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblMensaje.setFont(new Font("Arial", Font.BOLD, 17));
		lblMensaje.setForeground(new Color(250, 250, 210));
		lblMensaje.setBounds(66, 303, 558, 20);
		panel.add(lblMensaje);

		frmInspector.setTitle("INSPECTOR");

	}

	public void show() {
		this.frmInspector.setVisible(true);
	}

	public void cerrar() {
		this.frmInspector.setVisible(false);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public DefaultTableModel getModelInspector() {
		return modelInspector;
	}

	public JTable getTablaInspectores() {
		return tablaInspector;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public static void refrescarTabla() {
		inspectorService = new InspectorService();
		inspectorService.llenarTabla(modelInspector, nombreColumnas, inspectorEnTabla);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getBtnAgregar()) {
			this.ventanaAuxInspectores = new VentanaAuxInspector(this, "Agregar", null);
		}

		else if (e.getSource() == this.getBtnEditar()) {
			int[] filas_seleccionadas = this.getTablaInspectores().getSelectedRows();
			if (filas_seleccionadas.length == 1) {
				InspectorDto inspectorObtener = inspectorService.inspectorPorIdEnTabla(inspectorEnTabla,filas_seleccionadas[0]);
				this.ventanaAuxInspectores = new VentanaAuxInspector(this, "Editar", inspectorObtener);
			}

		}
		
		else if (e.getSource() == this.getBtnBorrar()) {
			int[] filas_seleccionadas = this.getTablaInspectores().getSelectedRows();
			for (int fila : filas_seleccionadas) {
				InspectorDto inspectorObtener = inspectorService.inspectorPorIdEnTabla(inspectorEnTabla,fila);
				inspectorService.eliminarInspector(inspectorObtener.getIdInspector());
			}
			refrescarTabla();
			;

		} else if (e.getSource() == this.ventanaAuxInspectores.getBtnAgregar()) {
			
			if (this.ventanaAuxInspectores.getDatos() != null) {
				InspectorDto nuevoPropietario = this.ventanaAuxInspectores.getDatos();
				inspectorService.agregarInspector(nuevoPropietario);
				refrescarTabla();
				this.ventanaAuxInspectores.dispose();
			}

		} else if (e.getSource() == this.ventanaAuxInspectores.getBtnEditar()) {
			if (this.ventanaAuxInspectores.getDatos() != null) {
				InspectorDto editarPropietario = this.ventanaAuxInspectores.getDatos();
				inspectorService.editarInspector(editarPropietario);
				refrescarTabla();
	
				this.ventanaAuxInspectores.dispose();
			}
		}

	}
}
