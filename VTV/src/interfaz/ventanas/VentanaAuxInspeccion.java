package interfaz.ventanas;

import javax.swing.JFrame;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Font;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import dto.Estado;
import dto.InspeccionDto;
import dto.VehiculoDto;
import interfaz.Inspecciones;
import service.InspeccionesService;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Color;

public class VentanaAuxInspeccion {

	public JFrame frmInspeccion;
	private JTextField textDominio, textModelo, textPropietario, textMarca;
	private ButtonGroup bgObservaciones = new ButtonGroup();
	private ButtonGroup bgMediciones = new ButtonGroup();
	private InspeccionesService inspeccionesService = new InspeccionesService();
	private static Estado resultadoObservaciones;
	private static Estado resultadoMediciones;
	public static JLabel lblResultado;
	private JRadioButton ObservacionApto, ObservacionCondicional, ObservacionRechazado, MedicionApto,
			MedicionCondicional, MedicionRechazado;
	private JButton btnFinalizar;
	private JRadioButton rdbtnExento;
	private JLabel lblFecha;
	private boolean exento;
	public static JLabel lblMensaje;

	public VentanaAuxInspeccion(String accion, InspeccionDto inspeccion) {
		frmInspeccion = new JFrame();
		frmInspeccion.getContentPane().setBackground(new Color(0, 128, 128));
		frmInspeccion.setTitle("INSPECCION");
		frmInspeccion.setBounds(100, 100, 700, 460);
		frmInspeccion.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmInspeccion.getContentPane().setLayout(null);

		rdbtnExento = new JRadioButton("EXENTO");
		rdbtnExento.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnExento.setBackground(new Color(32, 178, 170));
		rdbtnExento.setForeground(new Color(255, 255, 255));
		rdbtnExento.setFont(new Font("Arial", Font.BOLD, 15));
		rdbtnExento.setBounds(519, 246, 138, 29);
		frmInspeccion.getContentPane().add(rdbtnExento);

		GregorianCalendar gc = new GregorianCalendar();
		String fecha = gc.get(Calendar.DAY_OF_MONTH) + "/" + (gc.get(Calendar.MONTH) + 1) + "/" + gc.get(Calendar.YEAR);

		lblFecha = new JLabel("Fecha: " + fecha);
		lblFecha.setForeground(new Color(255, 255, 255));
		lblFecha.setFont(new Font("Arial", Font.BOLD, 15));
		lblFecha.setBounds(497, 54, 160, 31);
		frmInspeccion.getContentPane().add(lblFecha);

		JLabel lblDominio = new JLabel("Dominio");
		lblDominio.setForeground(new Color(255, 255, 255));
		lblDominio.setFont(new Font("Arial", Font.BOLD, 15));
		lblDominio.setBounds(43, 54, 98, 31);
		frmInspeccion.getContentPane().add(lblDominio);

		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setForeground(new Color(255, 255, 255));
		lblMarca.setFont(new Font("Arial", Font.BOLD, 15));
		lblMarca.setBounds(43, 117, 116, 31);
		frmInspeccion.getContentPane().add(lblMarca);

		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setForeground(new Color(255, 255, 255));
		lblModelo.setFont(new Font("Arial", Font.BOLD, 15));
		lblModelo.setBounds(43, 181, 116, 31);
		frmInspeccion.getContentPane().add(lblModelo);

		JLabel lblPropietario = new JLabel("Propietario");
		lblPropietario.setForeground(new Color(255, 255, 255));
		lblPropietario.setFont(new Font("Arial", Font.BOLD, 15));
		lblPropietario.setBounds(43, 246, 116, 31);
		frmInspeccion.getContentPane().add(lblPropietario);

		JLabel lblObservaciones = new JLabel("Observaciones");
		lblObservaciones.setForeground(new Color(255, 255, 255));
		lblObservaciones.setFont(new Font("Arial", Font.BOLD, 15));
		lblObservaciones.setBounds(88, 288, 177, 31);
		frmInspeccion.getContentPane().add(lblObservaciones);

		JLabel lblMediciones = new JLabel("Mediciones");
		lblMediciones.setForeground(new Color(255, 255, 255));
		lblMediciones.setFont(new Font("Arial", Font.BOLD, 15));
		lblMediciones.setBounds(307, 288, 177, 31);
		frmInspeccion.getContentPane().add(lblMediciones);

		textDominio = new JTextField();
		textDominio.setFont(new Font("Arial", Font.BOLD, 15));
		textDominio.setBounds(154, 49, 283, 41);
		frmInspeccion.getContentPane().add(textDominio);
		textDominio.setColumns(10);

		textMarca = new JTextField();
		textMarca.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textMarca.setColumns(10);
		textMarca.setBounds(154, 110, 283, 43);
		frmInspeccion.getContentPane().add(textMarca);

		textModelo = new JTextField();
		textModelo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textModelo.setColumns(10);
		textModelo.setBounds(154, 175, 283, 43);
		frmInspeccion.getContentPane().add(textModelo);

		textPropietario = new JTextField();
		textPropietario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textPropietario.setColumns(10);
		textPropietario.setBounds(154, 240, 283, 43);
		frmInspeccion.getContentPane().add(textPropietario);

		JLabel lblEstado = new JLabel("Estado:");
		lblEstado.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblEstado.setBounds(519, 298, 155, 14);
		frmInspeccion.getContentPane().add(lblEstado);

		lblResultado = new JLabel("");
		lblResultado.setForeground(new Color(255, 255, 255));
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setFont(new Font("Arial Black", Font.PLAIN, 15));
		lblResultado.setBounds(519, 316, 155, 31);
		frmInspeccion.getContentPane().add(lblResultado);

		radioButtonEstado();

		if (accion == "Agregar")
			inicializarAgregar();
		else
			inicializarEditar(inspeccion);

	}

	private void inicializarAgregar() {
		btnFinalizar = new JButton("Finalizar");
		btnFinalizar.setBackground(new Color(224, 255, 255));
		btnFinalizar.setForeground(new Color(128, 128, 128));
		btnFinalizar.setFont(new Font("Arial", Font.BOLD, 15));
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				inspeccionesService.nuevaInspeccion(textDominio.getText(), textPropietario.getText(),
						textMarca.getText(), textModelo.getText(), rdbtnExento.isSelected(), frmInspeccion);
				Inspecciones.refrescarTabla();
			}
		});
		btnFinalizar.setBounds(497, 362, 160, 48);
		frmInspeccion.getContentPane().add(btnFinalizar);

	}

	private void inicializarEditar(InspeccionDto inspeccion) {
		int idInspeccion = inspeccion.getIdInspeccion();
		lblFecha.setText("Fecha: " + inspeccion.getFecha());
		lblResultado.setText(inspeccion.getEstado().toString());
		rdbtnExento.setSelected(inspeccion.isExento());
		exento = rdbtnExento.isSelected();

		VehiculoDto vehiculoDto = inspeccionesService.buscarVehiculosPorInspeccion(inspeccion.getIdVehiculo());
		textDominio.setText(vehiculoDto.getDominio());
		textMarca.setText(vehiculoDto.getMarca());
		textModelo.setText(vehiculoDto.getModelo());
		textPropietario.setText(vehiculoDto.getPropietario());

		rdbtnExento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exento = rdbtnExento.isSelected();
			}
		});

		btnFinalizar = new JButton("Finalizar");
		btnFinalizar.setBackground(new Color(224, 255, 255));
		btnFinalizar.setForeground(new Color(128, 128, 128));
		btnFinalizar.setFont(new Font("Arial", Font.BOLD, 15));
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inspeccionesService.editarInspeccion(idInspeccion, textDominio.getText(), textPropietario.getText(),
						textMarca.getText(), textModelo.getText(), exento, inspeccion.getIdVehiculo(), frmInspeccion);
				Inspecciones.refrescarTabla();
			}
		});
		btnFinalizar.setBounds(536, 362, 138, 48);
		frmInspeccion.getContentPane().add(btnFinalizar);

	}

	public void radioButtonEstado() {
		ObservacionApto = new JRadioButton("APTO");
		ObservacionApto.setForeground(new Color(255, 255, 255));
		ObservacionApto.setHorizontalAlignment(SwingConstants.LEFT);
		ObservacionApto.setFont(new Font("Arial", Font.BOLD, 12));
		ObservacionApto.setBackground(new Color(32, 178, 170));
		ObservacionApto.setBounds(109, 322, 109, 23);
		ObservacionApto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ObservacionApto.isSelected()) {
					resultadoObservaciones = Estado.APTO;
					actualizarResultado();
				}
			}
		});
		frmInspeccion.getContentPane().add(ObservacionApto);

		ObservacionCondicional = new JRadioButton("CONDICIONAL");
		ObservacionCondicional.setForeground(new Color(255, 255, 255));
		ObservacionCondicional.setFont(new Font("Arial", Font.BOLD, 12));
		ObservacionCondicional.setBackground(new Color(32, 178, 170));
		ObservacionCondicional.setBounds(109, 348, 109, 23);
		ObservacionCondicional.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ObservacionCondicional.isSelected()) {
					resultadoObservaciones = Estado.CONDICIONAL;
					actualizarResultado();
				}
			}
		});
		frmInspeccion.getContentPane().add(ObservacionCondicional);

		ObservacionRechazado = new JRadioButton("RECHAZADO");
		ObservacionRechazado.setBackground(new Color(32, 178, 170));
		ObservacionRechazado.setForeground(new Color(255, 255, 255));
		ObservacionRechazado.setFont(new Font("Arial", Font.BOLD, 12));
		ObservacionRechazado.setBounds(109, 374, 109, 23);
		ObservacionRechazado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ObservacionRechazado.isSelected()) {
					resultadoObservaciones = Estado.RECHAZADO;
					actualizarResultado();
				}
			}
		});
		frmInspeccion.getContentPane().add(ObservacionRechazado);

		MedicionApto = new JRadioButton("APTO");
		MedicionApto.setForeground(new Color(255, 255, 255));
		MedicionApto.setFont(new Font("Arial", Font.BOLD, 12));
		MedicionApto.setBackground(new Color(32, 178, 170));
		MedicionApto.setBounds(328, 322, 109, 23);
		MedicionApto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (MedicionApto.isSelected()) {
					resultadoMediciones = Estado.APTO;
					actualizarResultado();
				}
			}
		});
		frmInspeccion.getContentPane().add(MedicionApto);

		MedicionCondicional = new JRadioButton("CONDICIONAL");
		MedicionCondicional.setBackground(new Color(32, 178, 170));
		MedicionCondicional.setForeground(new Color(255, 255, 255));
		MedicionCondicional.setFont(new Font("Arial", Font.BOLD, 12));
		MedicionCondicional.setBounds(328, 348, 109, 23);
		MedicionCondicional.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (MedicionCondicional.isSelected()) {
					resultadoMediciones = Estado.CONDICIONAL;
					actualizarResultado();
				}
			}
		});
		frmInspeccion.getContentPane().add(MedicionCondicional);

		MedicionRechazado = new JRadioButton("RECHAZADO");
		MedicionRechazado.setForeground(new Color(255, 255, 255));
		MedicionRechazado.setFont(new Font("Arial", Font.BOLD, 12));
		MedicionRechazado.setBackground(new Color(32, 178, 170));
		MedicionRechazado.setBounds(328, 374, 109, 23);
		MedicionRechazado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (MedicionRechazado.isSelected()) {
					resultadoMediciones = Estado.RECHAZADO;
					actualizarResultado();
				}
			}
		});
		frmInspeccion.getContentPane().add(MedicionRechazado);

		bgObservaciones.add(ObservacionApto);
		bgObservaciones.add(ObservacionCondicional);
		bgObservaciones.add(ObservacionRechazado);
		bgMediciones.add(MedicionApto);
		bgMediciones.add(MedicionCondicional);
		bgMediciones.add(MedicionRechazado);

		lblMensaje = new JLabel("");
		lblMensaje.setFont(new Font("Arial", Font.BOLD, 12));
		lblMensaje.setForeground(new Color(255, 255, 224));
		lblMensaje.setBounds(447, 121, 236, 14);
		frmInspeccion.getContentPane().add(lblMensaje);
	}

	public void actualizarResultado() {
		inspeccionesService.resultadoEstado(resultadoObservaciones, resultadoMediciones);

	}
}
