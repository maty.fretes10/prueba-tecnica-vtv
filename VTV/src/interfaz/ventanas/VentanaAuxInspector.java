package interfaz.ventanas;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import dto.InspectorDto;
import service.InspectorService;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

public class VentanaAuxInspector extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private int idInspector;
	private JTextField txtNombre;
	private JButton btnAgregar;
	private JButton btnEditar;
	private ActionListener controlador;
	public static JLabel lblMensaje;
	InspectorService inspectorService;

	public VentanaAuxInspector(ActionListener controlador, String accion, InspectorDto inspector) {
		super();
		setResizable(false);
		this.controlador = controlador;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 350, 150);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(32, 178, 170));
		panel.setBounds(0, 0, 344, 121);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 15));
		lblNombre.setBounds(10, 16, 95, 17);
		panel.add(lblNombre);

		idInspector = 0;

		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Arial", Font.BOLD, 15));
		txtNombre.setBounds(113, 8, 184, 34);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		lblMensaje = new JLabel("");
		lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblMensaje.setFont(new Font("Arial", Font.BOLD, 15));
		lblMensaje.setForeground(new Color(255, 255, 224));
		lblMensaje.setBounds(20, 44, 304, 17);
		panel.add(lblMensaje);

		inspectorService = new InspectorService();

		if (accion == "Agregar")
			inicializarAgregar(panel);
		else
			inicializarEditar(panel, inspector);

		this.setVisible(true);

	}

	private void inicializarAgregar(JPanel panel) {
		this.setTitle("Agregar Inspector");

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBackground(new Color(224, 255, 255));
		btnAgregar.setForeground(new Color(105, 105, 105));
		btnAgregar.setFont(new Font("Arial", Font.BOLD, 15));
		btnAgregar.addActionListener(this.controlador);
		btnAgregar.setBounds(173, 72, 141, 27);
		panel.add(btnAgregar);

	}

	private void inicializarEditar(JPanel panel, InspectorDto inspector) {
		this.setTitle("Editar Inspector");

		idInspector = inspector.getIdInspector();
		txtNombre.setText(inspector.getNombre());
		btnEditar = new JButton("Actualizar");
		btnEditar.setBackground(new Color(224, 255, 255));
		btnEditar.setForeground(new Color(105, 105, 105));
		btnEditar.setFont(new Font("Arial", Font.BOLD, 15));
		btnEditar.addActionListener(this.controlador);
		btnEditar.setBounds(173, 72, 141, 27);
		panel.add(btnEditar);
	}

	public int getIdInspector() {
		return idInspector;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public InspectorDto getDatos() {
		if (inspectorService.validarCampos(this.getTxtNombre().getText())) {
			InspectorDto tipoContacto = new InspectorDto(idInspector, this.getTxtNombre().getText());

			return tipoContacto;
		}
		return null;
	}
}
