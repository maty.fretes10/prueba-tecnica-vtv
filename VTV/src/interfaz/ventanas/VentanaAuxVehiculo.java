package interfaz.ventanas;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JTextField;

import dto.Estado;
import dto.InspectorDto;
import dto.VehiculoDto;
import interfaz.Vehiculos;
import service.VehiculoService;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JCalendar;
import javax.swing.JComboBox;
import java.awt.Color;

public class VentanaAuxVehiculo extends JFrame {

	JFrame frame;
	private JTextField textDominio;
	private JTextField textMarca;
	private JTextField textModelo;
	private JTextField textPropietario;
	private JButton btnAgregar;
	private JButton btnEditar;
	private static final long serialVersionUID = 1L;
	private int idVehiculo;
	private JTextField txtNombre;
	VehiculoService vehiculoService;
	JCalendar calendar;
	@SuppressWarnings("rawtypes")
	JComboBox cbEstado;
	public static JLabel lblMensaje;

	/**
	 * Create the application.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public VentanaAuxVehiculo(String accion, VehiculoDto vehiculo) {

		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 128, 128));
		frame.setBounds(100, 100, 700, 471);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblDominio = new JLabel("Dominio");
		lblDominio.setForeground(new Color(255, 255, 255));
		lblDominio.setFont(new Font("Arial", Font.BOLD, 15));
		lblDominio.setBounds(50, 9, 145, 40);
		frame.getContentPane().add(lblDominio);

		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setForeground(new Color(255, 255, 255));
		lblMarca.setFont(new Font("Arial", Font.BOLD, 15));
		lblMarca.setBounds(50, 66, 145, 40);
		frame.getContentPane().add(lblMarca);

		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setForeground(new Color(255, 255, 255));
		lblModelo.setFont(new Font("Arial", Font.BOLD, 15));
		lblModelo.setBounds(50, 117, 145, 40);
		frame.getContentPane().add(lblModelo);

		JLabel lblPropietario = new JLabel("Propietario");
		lblPropietario.setForeground(new Color(255, 255, 255));
		lblPropietario.setFont(new Font("Arial", Font.BOLD, 15));
		lblPropietario.setBounds(50, 168, 145, 40);
		frame.getContentPane().add(lblPropietario);

		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setForeground(new Color(255, 255, 255));
		lblEstado.setFont(new Font("Arial", Font.BOLD, 15));
		lblEstado.setBounds(50, 219, 145, 40);
		frame.getContentPane().add(lblEstado);

		JLabel lblVencimientoVtv = new JLabel("Vencimiento VTV");
		lblVencimientoVtv.setForeground(new Color(255, 255, 255));
		lblVencimientoVtv.setFont(new Font("Arial", Font.BOLD, 15));
		lblVencimientoVtv.setBounds(50, 270, 145, 40);
		frame.getContentPane().add(lblVencimientoVtv);

		textDominio = new JTextField();
		textDominio.setFont(new Font("Arial", Font.BOLD, 15));
		textDominio.setBackground(new Color(224, 255, 255));
		textDominio.setBounds(216, 11, 232, 40);
		frame.getContentPane().add(textDominio);
		textDominio.setColumns(10);

		textMarca = new JTextField();
		textMarca.setForeground(new Color(105, 105, 105));
		textMarca.setFont(new Font("Arial", Font.BOLD, 15));
		textMarca.setBackground(new Color(224, 255, 255));
		textMarca.setColumns(10);
		textMarca.setBounds(216, 68, 232, 40);
		frame.getContentPane().add(textMarca);

		textModelo = new JTextField();
		textModelo.setForeground(new Color(105, 105, 105));
		textModelo.setFont(new Font("Arial", Font.BOLD, 15));
		textModelo.setBackground(new Color(224, 255, 255));
		textModelo.setColumns(10);
		textModelo.setBounds(216, 117, 232, 40);
		frame.getContentPane().add(textModelo);

		textPropietario = new JTextField();
		textPropietario.setForeground(new Color(105, 105, 105));
		textPropietario.setFont(new Font("Arial", Font.BOLD, 15));
		textPropietario.setBackground(new Color(224, 255, 255));
		textPropietario.setColumns(10);
		textPropietario.setBounds(216, 169, 232, 40);
		frame.getContentPane().add(textPropietario);

		calendar = new JCalendar();
		calendar.setBounds(216, 268, 205, 153);
		frame.getContentPane().add(calendar);

		cbEstado = new JComboBox();
		cbEstado.setForeground(new Color(105, 105, 105));
		cbEstado.setFont(new Font("Arial", Font.BOLD, 15));
		cbEstado.setBackground(new Color(224, 255, 255));
		cbEstado.setBounds(216, 219, 232, 40);
		String[] model = { Estado.APTO.toString(), Estado.CONDICIONAL.toString(), Estado.RECHAZADO.toString() };
		DefaultComboBoxModel boxModel = new DefaultComboBoxModel(model);
		cbEstado.setModel(boxModel);
		frame.getContentPane().add(cbEstado);
		
		lblMensaje = new JLabel("");
		lblMensaje.setFont(new Font("Arial", Font.BOLD, 13));
		lblMensaje.setForeground(new Color(255, 250, 240));
		lblMensaje.setBounds(450, 285, 224, 70);
		frame.getContentPane().add(lblMensaje);

		vehiculoService = new VehiculoService();

		if (accion == "Agregar")
			inicializarAgregar();
		else
			inicializarEditar(vehiculo);

		this.frame.setVisible(true);
		
	}

	private void inicializarAgregar() {
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBackground(new Color(224, 255, 255));
		btnAgregar.setForeground(new Color(105, 105, 105));
		btnAgregar.setFont(new Font("Arial", Font.BOLD, 15));
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String vto = calendar.getCalendar().get(Calendar.DAY_OF_MONTH) + "/"
						+ (calendar.getCalendar().get(Calendar.MONTH) + 1) + "/"
						+ calendar.getCalendar().get(Calendar.YEAR);
				
				boolean camposValidos = vehiculoService.validarCampos(0, textDominio.getText(), textPropietario.getText(),
						cbEstado.getSelectedItem().toString(), textMarca.getText(), textModelo.getText(), vto);
				
				if (camposValidos) {
					System.out.println(camposValidos);
					vehiculoService.agregarVehiculo(textDominio.getText(), textPropietario.getText(),
							cbEstado.getSelectedItem().toString(), textMarca.getText(), textModelo.getText(), vto);
					Vehiculos.refrescarTabla();
					frame.setVisible(false);
				}
		
				

			}
		});

		btnAgregar.setBounds(460, 346, 202, 64);

		this.frame.getContentPane().add(btnAgregar);

	}

	private void inicializarEditar(VehiculoDto vehiculo) {

		idVehiculo = vehiculo.getIdVehiculo();
		textDominio.setText(vehiculo.getDominio());
		cbEstado.setSelectedItem(vehiculo.getEstado().toString());

		textMarca.setText(vehiculo.getMarca());
		textModelo.setText(vehiculo.getModelo());
		textPropietario.setText(vehiculo.getPropietario());

		// manipulacion de cadenas
		int index = vehiculo.getVto().indexOf("/");
		int index2 = vehiculo.getVto().lastIndexOf("/");
		int dia = Integer.parseInt(vehiculo.getVto().substring(0, index));
		int mes = Integer.parseInt(vehiculo.getVto().substring(index + 1, index2));
		int anio = Integer.parseInt(vehiculo.getVto().substring(index2 + 1));

		calendar.setCalendar(new GregorianCalendar(anio, mes - 1, dia));

		btnEditar = new JButton("Actualizar");
		btnEditar.setBackground(new Color(224, 255, 255));
		btnEditar.setForeground(new Color(105, 105, 105));
		btnEditar.setFont(new Font("Arial", Font.BOLD, 15));
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String vto = calendar.getCalendar().get(Calendar.DAY_OF_MONTH) + "/"
						+ (calendar.getCalendar().get(Calendar.MONTH) + 1) + "/"
						+ calendar.getCalendar().get(Calendar.YEAR);
				
				boolean camposValidos = vehiculoService.validarCampos(0, textDominio.getText(), textPropietario.getText(),
						cbEstado.getSelectedItem().toString(), textMarca.getText(), textModelo.getText(), vto);
				
				if (camposValidos) {
					vehiculoService.editarVehiculo(idVehiculo, textDominio.getText(), textPropietario.getText(),
							cbEstado.getSelectedItem().toString(), textMarca.getText(), textModelo.getText(), vto);
					Vehiculos.refrescarTabla();
					frame.setVisible(false);
				}
			}
		});
		btnEditar.setBounds(517, 346, 145, 64);
		this.frame.getContentPane().add(btnEditar);
	}

	public int getIdVehiculo() {
		return idVehiculo;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public InspectorDto getDatos() {
		InspectorDto tipoContacto = new InspectorDto(idVehiculo, this.getTxtNombre().getText());
		return tipoContacto;
	}
}
