package interfaz;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import dto.InspectorDto;
import persistencia.Conexion;
import persistencia.dao.sql.InspectorDAOSQL;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Vtv {

	public JFrame frame;
	private static JComboBox<InspectorDto> cbInspectores;
	public static InspectorDto inspector;

	/**
	 * Create the application.
	 */
	public Vtv() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(224, 255, 255));
		frame.setBounds(100, 100, 700, 460);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("VTV");
		lblNewLabel.setForeground(new Color(72, 209, 204));
		lblNewLabel.setFont(new Font("Arial Black", Font.BOLD, 72));
		lblNewLabel.setBounds(56, 93, 190, 116);
		frame.getContentPane().add(lblNewLabel);

		JButton btnInspecciones = new JButton("Inspecciones");
		btnInspecciones.setForeground(new Color(105, 105, 105));
		btnInspecciones.setFont(new Font("Arial", Font.BOLD, 15));
		btnInspecciones.setBackground(new Color(224, 255, 255));
		btnInspecciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Inspecciones window = new Inspecciones();
				window.frame.setVisible(true);
			}
		});
		btnInspecciones.setBounds(328, 139, 334, 74);
		frame.getContentPane().add(btnInspecciones);

		JButton btnVehiculos = new JButton("Vehiculos");
		btnVehiculos.setForeground(new Color(105, 105, 105));
		btnVehiculos.setFont(new Font("Arial", Font.BOLD, 15));
		btnVehiculos.setBackground(new Color(224, 255, 255));
		btnVehiculos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Vehiculos window = new Vehiculos();
				window.frmVehiculos.setVisible(true);
			}
		});
		btnVehiculos.setBounds(328, 236, 334, 74);
		frame.getContentPane().add(btnVehiculos);

		cbInspectores = new JComboBox<InspectorDto>();
		cbInspectores.setForeground(new Color(105,105,105));
		cbInspectores.setFont(new Font("Arial", Font.BOLD, 15));
		cbInspectores.setBackground(new Color(224, 255, 255));
		cbInspectores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inspector = (InspectorDto) cbInspectores.getSelectedItem();
			}
		});
		cbInspectores.setBounds(328, 53, 206, 30);
		ActualizarInspectores();

		frame.getContentPane().add(cbInspectores);

		if (inspector == null) {
			cbInspectores.setSelectedIndex(0);
			inspector = (InspectorDto) cbInspectores.getSelectedItem();
		}

		JButton btnNewButton = new JButton("Inspectores");
		btnNewButton.setForeground(new Color(105, 105, 105));
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 13));
		btnNewButton.setBackground(new Color(224, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Inspector propietario = new Inspector();
				propietario.frmInspector.setVisible(true);
			}
		});
		btnNewButton.setBounds(544, 53, 118, 30);
		frame.getContentPane().add(btnNewButton);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 128, 128));
		panel.setBounds(278, 0, 406, 421);
		frame.getContentPane().add(panel);
		
		JLabel lblNewLabel_1 = new JLabel("VERIFICACION TECNICA");
		lblNewLabel_1.setForeground(new Color(105, 105, 105));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 20));
		lblNewLabel_1.setBounds(0, 188, 279, 88);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("VEHICULAR");
		lblNewLabel_1_1.setForeground(new Color(105,105,105));
		lblNewLabel_1_1.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_1.setFont(new Font("Arial", Font.BOLD, 20));
		lblNewLabel_1_1.setBounds(0, 251, 279, 59);
		frame.getContentPane().add(lblNewLabel_1_1);
	}

	public void show() {
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(null, "Estas seguro que quieres salir?", "Confirmacion",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					Conexion.getConexion().cerrarConexion();
					System.exit(0);
				}
			}
		});
		this.frame.setVisible(true);
	}

	@SuppressWarnings("unchecked")
	public static void ActualizarInspectores() {
		Vector<InspectorDto> inspectores = new Vector<InspectorDto>();
		InspectorDAOSQL inspectorDAOSQL = new InspectorDAOSQL();
		inspectores.addAll(inspectorDAOSQL.readAll());

		@SuppressWarnings({ "rawtypes" })
		DefaultComboBoxModel modeloEtiqueta = new DefaultComboBoxModel(inspectores);
		cbInspectores.setModel(modeloEtiqueta);
	}
}
