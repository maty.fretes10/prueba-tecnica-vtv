package interfaz;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import dto.InspeccionDto;
import interfaz.ventanas.VentanaAuxInspeccion;
import service.InspeccionesService;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;

public class Inspecciones {

	public JFrame frame;
	private static DefaultTableModel modelInspecciones;
	private static String[] nombreColumnas = {"Numero","Fecha","Estado","Exento","Inspector","Dominio", "Propietario"};
	private static List<InspeccionDto> inspeccionesEnTabla;
	private JTable tablaInspecciones;


	static InspeccionesService inspeccionesService;

	/**
	 * Create the application.
	 */
	public Inspecciones() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 128, 128));
		frame.setBounds(100, 100, 700, 460);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBackground(new Color(224, 255, 255));
		btnEliminar.setForeground(new Color(105, 105, 105));
		btnEliminar.setFont(new Font("Arial", Font.BOLD, 15));
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] filas_seleccionadas = getTablaInspecciones().getSelectedRows();
				if (filas_seleccionadas.length == 1) {
					InspeccionDto inspeccion = inspeccionesService.inspeccionPorIdEnTabla(inspeccionesEnTabla,
							filas_seleccionadas[0]);
					inspeccionesService.eliminarInspeccion(inspeccion.getIdInspeccion());
					refrescarTabla();
				}
			}
		});
		btnEliminar.setBounds(165, 341, 165, 42);
		frame.getContentPane().add(btnEliminar);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.setForeground(new Color(105, 105, 105));
		btnEditar.setFont(new Font("Arial", Font.BOLD, 15));
		btnEditar.setBackground(new Color(224, 255, 255));
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] filas_seleccionadas = getTablaInspecciones().getSelectedRows();
				if (filas_seleccionadas.length == 1) {
					InspeccionDto inspeccion = inspeccionesService.inspeccionPorIdEnTabla(inspeccionesEnTabla, filas_seleccionadas[0]);
					VentanaAuxInspeccion window = new VentanaAuxInspeccion("Editar", inspeccion);
					window.frmInspeccion.setVisible(true);
				}
			}
		});
		btnEditar.setBounds(340, 341, 159, 42);
		frame.getContentPane().add(btnEditar);
		
		JButton btnAgregar = new JButton("Nueva Inspeccion");
		btnAgregar.setBackground(new Color(224, 255, 255));
		btnAgregar.setForeground(new Color(105, 105, 105));
		btnAgregar.setFont(new Font("Arial", Font.BOLD, 15));
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaAuxInspeccion window = new VentanaAuxInspeccion("Agregar", null);
				window.frmInspeccion.setVisible(true);
			}
		});
		btnAgregar.setBounds(509, 341, 165, 42);
		frame.getContentPane().add(btnAgregar);
		
		JScrollPane spInscripciones = new JScrollPane();
		spInscripciones.setBounds(10, 77, 664, 241);
		frame.getContentPane().add(spInscripciones);
		
		modelInspecciones = new DefaultTableModel(null,nombreColumnas);
		tablaInspecciones = new JTable(modelInspecciones);
		tablaInspecciones.setForeground(new Color(105, 105, 105));
		tablaInspecciones.setFont(new Font("Arial", Font.BOLD, 12));
		
		tablaInspecciones.getColumnModel().getColumn(0).setPreferredWidth(130);
		tablaInspecciones.getColumnModel().getColumn(0).setResizable(false);
		
		refrescarTabla();
		
		spInscripciones.setViewportView(tablaInspecciones);
		
		JTextPane textBuscadorProp = new JTextPane();
		textBuscadorProp.setForeground(new Color(105, 105, 105));
		textBuscadorProp.setBackground(new Color(224, 255, 255));
		textBuscadorProp.setFont(new Font("Arial", Font.BOLD, 15));
		textBuscadorProp.setBounds(378, 24, 200, 24);
		frame.getContentPane().add(textBuscadorProp);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBackground(new Color(224, 255, 255));
		btnBuscar.setForeground(new Color(105, 105, 105));
		btnBuscar.setFont(new Font("Arial", Font.BOLD, 15));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inspeccionesEnTabla = inspeccionesService.buscarVehiculosPorProp(textBuscadorProp.getText());
				refrescarTabla();
			}
		});
		btnBuscar.setBounds(588, 24, 86, 24);
		frame.getContentPane().add(btnBuscar);
		
		JLabel lblNewLabel = new JLabel("Propietario: ");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setBounds(283, 24, 114, 24);
		frame.getContentPane().add(lblNewLabel);

	}
	
	public JTable getTablaInspecciones() {
		return tablaInspecciones;
	}

	public void setTablaInspecciones(JTable tablaInspecciones) {
		this.tablaInspecciones = tablaInspecciones;
	}
	
	public static void refrescarTabla() {
		inspeccionesService = new InspeccionesService();
		inspeccionesService.llenarTabla(modelInspecciones, nombreColumnas, inspeccionesEnTabla);
	}
}
