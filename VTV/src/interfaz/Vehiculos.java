package interfaz;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.Estado;
import dto.VehiculoDto;
import interfaz.ventanas.VentanaAuxVehiculo;
import service.VehiculoService;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

public class Vehiculos {

	JFrame frmVehiculos;

	private static DefaultTableModel modelVehiculos;
	private static List<VehiculoDto> vehiculoEnTabla;
	private static String[] nombreColumnas = { "Dominio", "Marca", "Modelo", "Propietario", "Estado", "Vencimiento" };
	private JTable tablaVehiculos;
	@SuppressWarnings("unused")
	private VentanaAuxVehiculo ventana;
	static VehiculoService vehiculoService;
	private ButtonGroup bgEstado = new ButtonGroup();
	private JRadioButton rdbtnApto, rdbtnCondicional, rdbtnRechazado, rdbtnTodos;
	public static JLabel lblMensaje;

	public Vehiculos() {
		initialize();
	}

	private void initialize() {
		frmVehiculos = new JFrame();
		frmVehiculos.getContentPane().setBackground(new Color(0, 128, 128));
		frmVehiculos.setTitle("Vehiculos");
		frmVehiculos.setBounds(100, 100, 700, 460);
		frmVehiculos.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmVehiculos.getContentPane().setLayout(null);

		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBackground(new Color(224, 255, 255));
		btnAgregar.setForeground(new Color(105, 105, 105));
		btnAgregar.setFont(new Font("Arial", Font.BOLD, 15));
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ventana = new VentanaAuxVehiculo("Agregar", null);
				refrescarTabla();
			}
		});
		btnAgregar.setBounds(524, 345, 150, 42);
		frmVehiculos.getContentPane().add(btnAgregar);

		JButton btnEditar = new JButton("Editar");
		btnEditar.setBackground(new Color(224, 255, 255));
		btnEditar.setForeground(new Color(105, 105, 105));
		btnEditar.setFont(new Font("Arial", Font.BOLD, 15));
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int[] filas_seleccionadas = getTablaVehiculos().getSelectedRows();
				if (filas_seleccionadas.length == 1) {
					VehiculoDto vehiculo = vehiculoService.vehiculoPorIdEnTabla(vehiculoEnTabla,
							filas_seleccionadas[0]);
					ventana = new VentanaAuxVehiculo("Editar", vehiculo);
				}
			}
		});
		btnEditar.setBounds(364, 345, 150, 42);
		frmVehiculos.getContentPane().add(btnEditar);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBackground(new Color(224, 255, 255));
		btnEliminar.setForeground(new Color(105, 105, 105));
		btnEliminar.setFont(new Font("Arial", Font.BOLD, 15));
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] filas_seleccionadas = getTablaVehiculos().getSelectedRows();
				if (filas_seleccionadas.length == 1) {
					VehiculoDto vehiculo = vehiculoService.vehiculoPorIdEnTabla(vehiculoEnTabla,
							filas_seleccionadas[0]);
					vehiculoService.eliminarVehiculo(vehiculo.getIdVehiculo());
					refrescarTabla();
				}
			}
		});
		btnEliminar.setBounds(203, 345, 150, 42);
		frmVehiculos.getContentPane().add(btnEliminar);

		JScrollPane spVehiculos = new JScrollPane();
		spVehiculos.setBounds(10, 60, 664, 248);
		frmVehiculos.getContentPane().add(spVehiculos);

		modelVehiculos = new DefaultTableModel(null, nombreColumnas);
		tablaVehiculos = new JTable(modelVehiculos);
		tablaVehiculos.setForeground(new Color(105, 105, 105));
		tablaVehiculos.setFont(new Font("Arial", Font.BOLD, 13));
		tablaVehiculos.setBackground(new Color(255, 255, 255));

		tablaVehiculos.getColumnModel().getColumn(0).setPreferredWidth(130);
		tablaVehiculos.getColumnModel().getColumn(0).setResizable(false);	

		spVehiculos.setViewportView(tablaVehiculos);

		rdbtnApto = new JRadioButton("APTO");
		rdbtnApto.setBackground(new Color(32, 178, 170));
		rdbtnApto.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnApto.setForeground(new Color(255, 255, 255));
		rdbtnApto.setFont(new Font("Arial", Font.BOLD, 15));
		rdbtnApto.setBounds(191, 21, 142, 23);
		rdbtnApto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnApto.isSelected()) {
					vehiculoEnTabla = vehiculoService.vehiculoPorEstado(Estado.valueOf("APTO"));
					refrescarTabla();
				}
			}
		});
		frmVehiculos.getContentPane().add(rdbtnApto);

		rdbtnCondicional = new JRadioButton("CONDICIONAL");
		rdbtnCondicional.setBackground(new Color(32, 178, 170));
		rdbtnCondicional.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnCondicional.setForeground(new Color(255, 255, 255));
		rdbtnCondicional.setFont(new Font("Arial", Font.BOLD, 15));
		rdbtnCondicional.setBounds(353, 21, 142, 23);
		rdbtnCondicional.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnCondicional.isSelected()) {
					vehiculoEnTabla = vehiculoService.vehiculoPorEstado(Estado.valueOf("CONDICIONAL"));
					refrescarTabla();
				}
			}
		});
		frmVehiculos.getContentPane().add(rdbtnCondicional);

		rdbtnRechazado = new JRadioButton("RECHAZADO");
		rdbtnRechazado.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnRechazado.setBackground(new Color(32, 178, 170));
		rdbtnRechazado.setForeground(new Color(255, 255, 255));
		rdbtnRechazado.setFont(new Font("Arial", Font.BOLD, 15));
		rdbtnRechazado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnRechazado.isSelected()) {
					vehiculoEnTabla = vehiculoService.vehiculoPorEstado(Estado.valueOf("RECHAZADO"));
					refrescarTabla();
				}
			}
		});
		rdbtnRechazado.setBounds(520, 21, 143, 23);
		frmVehiculos.getContentPane().add(rdbtnRechazado);

		rdbtnTodos = new JRadioButton("TODOS");
		rdbtnTodos.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnTodos.setBackground(new Color(32, 178, 170));
		rdbtnTodos.setForeground(Color.WHITE);
		rdbtnTodos.setFont(new Font("Arial", Font.BOLD, 15));
		rdbtnTodos.setSelected(true);
		rdbtnTodos.setBounds(23, 21, 142, 23);
		if (rdbtnTodos.isSelected()) {
			refrescarTabla();
		}
		rdbtnTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnTodos.isSelected()) {
					vehiculoEnTabla = null;
					refrescarTabla();
				}
			}
		});
		frmVehiculos.getContentPane().add(rdbtnTodos);

		bgEstado.add(rdbtnApto);
		bgEstado.add(rdbtnCondicional);
		bgEstado.add(rdbtnRechazado);
		bgEstado.add(rdbtnTodos);
		
		lblMensaje = new JLabel("");
		lblMensaje.setFont(new Font("Arial", Font.BOLD, 12));
		lblMensaje.setForeground(new Color(255, 255, 240));
		lblMensaje.setBounds(96, 310, 521, 23);
		frmVehiculos.getContentPane().add(lblMensaje);
	}

	public JTable getTablaVehiculos() {
		return tablaVehiculos;
	}

	public static void refrescarTabla() {
		vehiculoService = new VehiculoService();
		vehiculoService.llenarTabla(modelVehiculos, nombreColumnas, vehiculoEnTabla);
	}
}
