package service;

import java.util.List;
import javax.swing.table.DefaultTableModel;
import dto.InspectorDto;
import interfaz.Inspector;
import interfaz.ventanas.VentanaAuxInspector;
import persistencia.dao.sql.InspectorDAOSQL;

public class InspectorService {
	InspectorDAOSQL inspectorRepository;

	public InspectorService() {
		inspectorRepository = new InspectorDAOSQL();
	}
	
	public void agregarInspector(InspectorDto inspector) {
		inspectorRepository.insert(inspector);
	}
	
	public void editarInspector(InspectorDto inspector) {
		inspectorRepository.update(inspector);
	}

	public InspectorDto inspectorPorId(int id) {
		return inspectorRepository.getById(id);
	}
	
	public InspectorDto inspectorPorIdEnTabla(List<InspectorDto> inspectorEnTabla, int filaSeleccionada) {
		inspectorEnTabla = inspectorRepository.readAll();
		return inspectorRepository.getById(inspectorEnTabla.get(filaSeleccionada).getIdInspector());
	}
	
	public boolean eliminarInspector(int id) {
		InspeccionesService inspeccionesService = new InspeccionesService();
		boolean tieneFK = inspeccionesService.buscarInspeccionPorInspector(id);
		boolean retorno = false;
		if (!tieneFK) {
			inspectorRepository.delete(id);
			Inspector.lblMensaje.setText("");
			retorno = true;
		} else {
			Inspector.lblMensaje.setText("Tiene inspecciones abiertas, primero debera eliminarlas.");
		}
		return retorno;
	}

	public void llenarTabla(DefaultTableModel modelInspector, String[] nombreColumnas,
			List<InspectorDto> inspectorEnTabla) {
		modelInspector.setRowCount(0); 
		modelInspector.setColumnCount(0);
		modelInspector.setColumnIdentifiers(nombreColumnas);

		inspectorEnTabla = inspectorRepository.readAll();
		for (int i = 0; i < inspectorEnTabla.size(); i++) {
			Object[] fila = { inspectorEnTabla.get(i).getNombre(), };
			modelInspector.addRow(fila);
		}
	}
	
	public boolean validarCampos(String nombre) {
		boolean retorno = true;
		if (nombre.isEmpty() ) {
			VentanaAuxInspector.lblMensaje.setText("Campo nombre es obligatorio");
			retorno = false;
		}

		return retorno;
	}
}
