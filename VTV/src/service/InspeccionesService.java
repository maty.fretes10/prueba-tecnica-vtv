package service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import dto.Estado;
import dto.InspeccionDto;
import dto.InspectorDto;
import dto.VehiculoDto;
import interfaz.Vtv;
import interfaz.ventanas.VentanaAuxInspeccion;
import persistencia.dao.sql.InspeccionDAOSQL;

public class InspeccionesService {
	InspeccionDAOSQL inspeccionRepository;
	VehiculoService vehiculoService;
	InspectorService inspectorService;

	public InspeccionesService() {
		inspeccionRepository = new InspeccionDAOSQL();
		vehiculoService = new VehiculoService();
		inspectorService = new InspectorService();
	}

	public void nuevaInspeccion(String dom, String prop, String marca, String modelo, boolean exento,
			JFrame frmInspeccion) {

		InspectorDto inspector = Vtv.inspector;

		String estadoFinal = VentanaAuxInspeccion.lblResultado.getText();

		String vencimiento = establecerVencimiento(estadoFinal);

		if (vehiculoService.validarCampos(1, dom, prop, estadoFinal.toString(), marca, modelo, vencimiento)) {
			int nuevoVehiculoID = vehiculoService.agregarVehiculo(dom, prop, estadoFinal.toString(), marca, modelo,
					vencimiento);

			String fechaActual = establecerVencimiento("RECHAZADO");
			InspeccionDto nuevaInspeccion = new InspeccionDto(0, fechaActual, Estado.valueOf(estadoFinal), exento,
					nuevoVehiculoID, inspector.getIdInspector());
			inspeccionRepository.insert(nuevaInspeccion);
			frmInspeccion.setVisible(false);
		}
	}

	public void editarInspeccion(int id, String dom, String prop, String marca, String modelo, boolean exento,
			int idVehiculo, JFrame frmInspeccion) {
		InspectorDto inspector = Vtv.inspector;
		Estado estadoFinal = Estado.valueOf(VentanaAuxInspeccion.lblResultado.getText());
		String vencimiento = establecerVencimiento(estadoFinal.toString());

		if (vehiculoService.validarCampos(1, dom, prop, estadoFinal.toString(), marca, modelo, vencimiento)) {
			vehiculoService.editarVehiculo(idVehiculo, dom, prop, estadoFinal.toString(), marca, modelo, vencimiento);
			String fechaActual = establecerVencimiento("RECHAZADO");
			InspeccionDto nuevaInspeccion = new InspeccionDto(id, fechaActual, estadoFinal, exento, idVehiculo,
					inspector.getIdInspector());
			inspeccionRepository.update(nuevaInspeccion);
			frmInspeccion.setVisible(false);
		}
	}

	public void eliminarInspeccion(int idInspeccion) {
		inspeccionRepository.delete(idInspeccion);
	}

	public String establecerVencimiento(String estadoFinal) {
		GregorianCalendar gc = new GregorianCalendar();
		int dia = gc.get(Calendar.DAY_OF_MONTH);
		int mes = gc.get(Calendar.MONTH) + 1;
		int anio = gc.get(Calendar.YEAR);

		if (!estadoFinal.isBlank() || estadoFinal != null) {
			if (estadoFinal.equals(Estado.APTO.toString())) {
				anio += 1;
			} else if (estadoFinal.equals(Estado.CONDICIONAL.toString())) {
				dia += 1;
			}
		}
		return dia + "/" + mes + "/" + anio;
	}

	public void llenarTabla(DefaultTableModel modelInspecciones, String[] nombreColumnas,
			List<InspeccionDto> inspeccionesEnTabla) {
		modelInspecciones.setRowCount(0);
		modelInspecciones.setColumnCount(0);
		modelInspecciones.setColumnIdentifiers(nombreColumnas);

		if (inspeccionesEnTabla == null || inspeccionesEnTabla.size() == 0) {
			inspeccionesEnTabla = inspeccionRepository.readAll();
		}

		for (int i = 0; i < inspeccionesEnTabla.size(); i++) {
			Object[] fila = { inspeccionesEnTabla.get(i).getIdInspeccion(), inspeccionesEnTabla.get(i).getFecha(),
					inspeccionesEnTabla.get(i).getEstado(), inspeccionesEnTabla.get(i).isExento(),
					inspectorService.inspectorPorId(inspeccionesEnTabla.get(i).getIdInspector()).getNombre(),
					vehiculoService.vehiculoPorId(inspeccionesEnTabla.get(i).getIdVehiculo()).getDominio(),
					vehiculoService.vehiculoPorId(inspeccionesEnTabla.get(i).getIdVehiculo()).getPropietario() };
			modelInspecciones.addRow(fila);
		}
	}

	public void resultadoEstado(Estado resultadoObservaciones, Estado resultadoMediciones) {
		if (resultadoObservaciones != null && resultadoMediciones != null) {
			if (resultadoObservaciones.equals(Estado.RECHAZADO) || resultadoMediciones.equals(Estado.RECHAZADO)) {
				VentanaAuxInspeccion.lblResultado.setText("RECHAZADO");
			} else if (resultadoObservaciones.equals(Estado.CONDICIONAL)
					|| resultadoMediciones.equals(Estado.CONDICIONAL)) {
				VentanaAuxInspeccion.lblResultado.setText("CONDICIONAL");
			} else if (resultadoObservaciones.equals(Estado.APTO) && resultadoMediciones.equals(Estado.APTO)) {
				VentanaAuxInspeccion.lblResultado.setText("APTO");
			}
		}
	}

	public List<InspeccionDto> buscarVehiculosPorProp(String text) {
		List<InspeccionDto> retorno = new ArrayList<>();
		List<InspeccionDto> listaInspecciones = inspeccionRepository.readAll();
		List<VehiculoDto> listaVehiculos = vehiculoService.vehiculoPorPropietario(text);

		for (InspeccionDto inspeccionDto : listaInspecciones) {
			for (VehiculoDto vehiculoDto : listaVehiculos) {
				if (inspeccionDto.getIdVehiculo() == vehiculoDto.getIdVehiculo()) {
					retorno.add(inspeccionDto);
				}

			}
		}
		return retorno;
	}

	public VehiculoDto buscarVehiculosPorInspeccion(int id) {
		return vehiculoService.vehiculoPorId(id);
	}

	public boolean buscarInspeccionPorVehiculo(int idVehiculo) {
		List<InspeccionDto> listaInspecciones = inspeccionRepository.readAll();
		boolean retorno = false;
		;

		for (InspeccionDto inspeccionDto : listaInspecciones) {
			if (inspeccionDto.getIdVehiculo() == idVehiculo) 
				retorno = true;
		}
		return retorno;
	}

	public InspeccionDto inspeccionPorIdEnTabla(List<InspeccionDto> inspeccionesEnTabla, int filaSeleccionada) {
		inspeccionesEnTabla = inspeccionRepository.readAll();
		return inspeccionRepository.getById(inspeccionesEnTabla.get(filaSeleccionada).getIdInspeccion());
	}

	public boolean buscarInspeccionPorInspector(int idInspector) {
		List<InspeccionDto> listaInspecciones = inspeccionRepository.readAll();
		boolean retorno = false;
		for (InspeccionDto inspeccionDto : listaInspecciones) {
			if (inspeccionDto.getIdInspector() == idInspector) 
				retorno = true;
		}
		return retorno;
	}

}
