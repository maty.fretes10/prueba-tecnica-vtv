package service;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import dto.Estado;
import dto.VehiculoDto;
import interfaz.Vehiculos;
import interfaz.ventanas.VentanaAuxInspeccion;
import interfaz.ventanas.VentanaAuxVehiculo;
import persistencia.dao.sql.VehiculoDAOSQL;

public class VehiculoService {
	VehiculoDAOSQL vehiculoRepository;

	public VehiculoService() {
		vehiculoRepository = new VehiculoDAOSQL();
	}

	public int agregarVehiculo(String dom, String prop, String estado, String marca, String modelo, String vto) {
		return vehiculoRepository.insert(new VehiculoDto(0, dom, prop, Estado.valueOf(estado), marca, modelo, vto));
	}

	public void editarVehiculo(int id, String dom, String prop, String estado, String marca, String modelo,
			String vto) {
		vehiculoRepository.update(new VehiculoDto(id, dom, prop, Estado.valueOf(estado), marca, modelo, vto));
	}

	public void eliminarVehiculo(int id) {
		InspeccionesService inspeccionesService = new InspeccionesService();
		boolean tieneFK = inspeccionesService.buscarInspeccionPorVehiculo(id);
		if (!tieneFK) {
			vehiculoRepository.delete(id);
			Vehiculos.lblMensaje.setText("");
		} else {
			Vehiculos.lblMensaje.setText("Tiene inspecciones abiertas, primero debera eliminarlas.");
		}
	}

	public VehiculoDto vehiculoPorId(int id) {
		return vehiculoRepository.getById(id);
	}

	public void llenarTabla(DefaultTableModel modelVehiculos, String[] nombreColumnas,
			List<VehiculoDto> vehiculoEnTabla) {
		modelVehiculos.setRowCount(0); // Para vaciar la tabla
		modelVehiculos.setColumnCount(0);
		modelVehiculos.setColumnIdentifiers(nombreColumnas);

		if (vehiculoEnTabla == null) {
			vehiculoEnTabla = vehiculoRepository.readAll();
		}

		for (int i = 0; i < vehiculoEnTabla.size(); i++) {
			Object[] fila = { vehiculoEnTabla.get(i).getDominio(), vehiculoEnTabla.get(i).getMarca(),
					vehiculoEnTabla.get(i).getModelo(), vehiculoEnTabla.get(i).getPropietario(),
					vehiculoEnTabla.get(i).getEstado(), vehiculoEnTabla.get(i).getVto() };
			modelVehiculos.addRow(fila);
		}
	}

	public VehiculoDto vehiculoPorIdEnTabla(List<VehiculoDto> vehiculoEnTabla, int filaSeleccionada) {
		vehiculoEnTabla = vehiculoRepository.readAll();
		return vehiculoRepository.getById(vehiculoEnTabla.get(filaSeleccionada).getIdVehiculo());
	}

	public List<VehiculoDto> vehiculoPorEstado(Estado estado) {
		List<VehiculoDto> todos = vehiculoRepository.readAll();
		List<VehiculoDto> vehiculosPorEstado = new ArrayList<>();

		for (VehiculoDto vehiculo : todos) {
			if (vehiculo.getEstado().equals(estado)) {
				vehiculosPorEstado.add(vehiculo);
			}
		}
		return vehiculosPorEstado;
	}

	public List<VehiculoDto> vehiculoPorPropietario(String prop) {
		List<VehiculoDto> todos = vehiculoRepository.readAll();
		List<VehiculoDto> vehiculosPorProp = new ArrayList<>();

		for (VehiculoDto vehiculo : todos) {
			if (vehiculo.getPropietario().toLowerCase().contains(prop.toLowerCase())) {
				vehiculosPorProp.add(vehiculo);
			}
		}
		return vehiculosPorProp;
	}

	public boolean validarCampos(int dondeViene, String dom, String prop, String estado, String marca, String modelo,
			String vto) {
		boolean retorno = true;
		if (dom.isEmpty() || prop.isEmpty() || estado.isEmpty() || marca.isEmpty() || modelo.isEmpty()
				|| vto.isEmpty()) {
			if (dondeViene == 0) {
				VentanaAuxVehiculo.lblMensaje.setText("Todos los campos son obligatorios");
			} else if (dondeViene == 1) {
				VentanaAuxInspeccion.lblMensaje.setText("Todos los campos son obligatorios");
			}
			retorno = false;
		}

		return retorno;
	}
}
