package test;

import static org.junit.Assert.*;

import org.junit.Test;

import service.VehiculoService;

public class VehiculosTest {

	@Test()
	public void validarCamposFalsoTest() {
		VehiculoService vehiculoService = new VehiculoService();
		assertFalse(vehiculoService.validarCampos(2, "", "Matias", "APTO", "Peugeot", "206", "27/4/2022"));
	}
	
	@Test()
	public void validarCamposVerdaderoTest() {
		VehiculoService vehiculoService = new VehiculoService();
		assertTrue(vehiculoService.validarCampos(0, "ABC123", "Matias", "APTO", "Peugeot", "206", "27/4/2022"));
	}
}
