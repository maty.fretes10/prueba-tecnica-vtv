package test;

import static org.junit.Assert.*;

import org.junit.Test;
import service.InspectorService;

public class InspectorTest {

	@Test
	public void validarCampoTest() {
		InspectorService inspectorService = new InspectorService();
		boolean result = inspectorService.validarCampos("Juan Perez");
		
		assertTrue(result);
	}

	@Test(expected = AssertionError.class)
	public void validarCampoErrorTest() {
		InspectorService inspectorService = new InspectorService();
		boolean result = inspectorService.validarCampos("Juan Perez");
		
		assertFalse(result);
	}

}
