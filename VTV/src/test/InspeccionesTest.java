package test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import service.InspeccionesService;

public class InspeccionesTest {

	public String fechaDeHoy() {
		GregorianCalendar gc = new GregorianCalendar();
		int dia = gc.get(Calendar.DAY_OF_MONTH);
		int mes = gc.get(Calendar.MONTH) + 1;
		int anio = gc.get(Calendar.YEAR);
		return dia + "/" + mes + "/" + anio;
	}
	
	@Test
	public void establecerVencimientoDeRechazadoTest() {
		InspeccionesService inspeccionesService = new InspeccionesService();
		String vtoRechazado = inspeccionesService.establecerVencimiento("RECHAZADO");
		
		assertEquals(fechaDeHoy(), vtoRechazado);
	}

	@Test
	public void establecerVencimientoDeCondicionalTest() {
		InspeccionesService inspeccionesService = new InspeccionesService();
		String vtoCondicional = inspeccionesService.establecerVencimiento("CONDICIONAL");
		
		assertFalse(vtoCondicional.equals(fechaDeHoy()));
	}
	
	@Test
	public void establecerVencimientoDeAptoTest() {
		InspeccionesService inspeccionesService = new InspeccionesService();
		String vtoApto = inspeccionesService.establecerVencimiento("APTO");
		
		assertFalse(vtoApto.equals(fechaDeHoy()));
	}
	
	@Test
	public void establecerVencimientoDeApto2Test() {
		InspeccionesService inspeccionesService = new InspeccionesService();
		String vtoApto = inspeccionesService.establecerVencimiento("APTO");
		GregorianCalendar gc = new GregorianCalendar();
		int dia = gc.get(Calendar.DAY_OF_MONTH);
		int mes = gc.get(Calendar.MONTH) + 1;
		int anio = gc.get(Calendar.YEAR) + 1;
		String fechaMasUnAnio = dia + "/" + mes + "/" + anio;
		
		assertTrue(vtoApto.equals(fechaMasUnAnio));
	}

	@Test
	public void establecerVencimientoDeCondicional2Test() {
		InspeccionesService inspeccionesService = new InspeccionesService();
		String vtoCondicional = inspeccionesService.establecerVencimiento("CONDICIONAL");
		GregorianCalendar gc = new GregorianCalendar();
		int dia = gc.get(Calendar.DAY_OF_MONTH) + 1;
		int mes = gc.get(Calendar.MONTH) + 1;
		int anio = gc.get(Calendar.YEAR);
		String fechaMasUnDia = dia + "/" + mes + "/" + anio;
		
		assertTrue(vtoCondicional.equals(fechaMasUnDia));
	}
	
}
